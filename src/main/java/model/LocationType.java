package model;

import java.util.ArrayList;
import java.util.List;

public class LocationType {

    private static List<String> locationTypes = new ArrayList<>();

    static {
        locationTypes.add("Bar");
        locationTypes.add("Museum");
        locationTypes.add("Restaurant");
        locationTypes.add("Hotel");
        locationTypes.add("Architectural monument");
    }

    public static List<String> getLocationTypes() {
        return locationTypes;
    }
}
