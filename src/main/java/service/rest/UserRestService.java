package service.rest;

import model.Location;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserWebService;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/users")
public class UserRestService extends UserWebService {

    private static Logger LOG = LogManager.getLogger(UserRestService.class);

    @POST
    @Path("/checkIn")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response changeUserLocation(User user) {
        if (userManager.getUsers().stream().noneMatch(x -> x.getName().equals(user.getName()))) {
            LOG.error("Try to change location for not existing user");
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Not found user with this name!").build();
        } else if (locationManager.getLocations().stream()
                .noneMatch(x -> x.getName().equalsIgnoreCase(user.getLocation().getName()) && x.getType().equalsIgnoreCase(user.getLocation().getType()))) {
            LOG.error("Try to change not found location for user ");
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Not found location what you want to change for user").build();
        } else {
            Location location = locationManager.getLocations()
                    .stream()
                    .filter(x -> x.getName().equalsIgnoreCase(user.getLocation().getName()) && x.getType().equalsIgnoreCase(user.getLocation().getType()))
                    .findFirst().get();
            user.setLocation(location);
            userManager.changeUserLocation(user);
            return Response
                    .status(Response.Status.OK)
                    .entity(user).build();
        }
    }


}
