package service.rest;

import model.Location;
import model.LocationType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.LocationWebService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

@Path("/locations")
public class LocationRestService extends LocationWebService {
    private static Logger LOG = LogManager.getLogger(LocationRestService.class);

    @GET
    @Path("/getAllLocations")
    @Produces("application/json")
    public Response getAllLocations() {
        return Response
                .status(Response.Status.OK)
                .entity(locationManager.readLocation()).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/createLocation")
    public Response createLocation(
            @QueryParam("type") String type,
            @QueryParam("name") String name,
            @QueryParam("xCoordinate") int x,
            @QueryParam("yCoordinate") int y) {
        Location location = new Location(type, name, x, y);
        locationManager.addNewLocation(location);
        return Response
                .status(Response.Status.CREATED)
                .entity(location).build();
    }

    @POST
    @Path("/createLocation")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addLocation(Location location) {
        if (Objects.isNull(location)) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("Please add location details !!")
                    .build();
        } else if (LocationType.getLocationTypes().stream().noneMatch(type -> type.equals(location.getType()))) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("Provide one of the following locations type: " + LocationType.getLocationTypes())
                    .build();
        } else if (location.getName() == null) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("Please provide the location name !!")
                    .build();
        } else if (location.getxCoordinate() < 0 || location.getyCoordinate() < 0) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("Please specify correct location coordinate, it should be more then zero")
                    .build();
        } else {
            locationManager.addNewLocation(location);
            return Response
                    .status(Response.Status.CREATED)
                    .entity(location).build();
        }
    }

}
