package service;

import model.User;
import util.UserManager;

import javax.ws.rs.core.Response;

public abstract class UserWebService extends BaseService{

    public abstract Response changeUserLocation(User user);

}
