package service;

import model.Location;

import javax.ws.rs.core.Response;

public abstract class LocationWebService extends BaseService {

    public abstract Response getAllLocations();

    public abstract Response createLocation(String type, String name, int x, int y);

    public abstract Response addLocation(Location location);
}
