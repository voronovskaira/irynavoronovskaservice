package util;

import model.Location;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LocationManager {

    private List<Location> locations;

    public LocationManager() {
        this.locations = readLocation();
    }

    public List<Location> readLocation() {
        locations = new ArrayList<>();
        try {
            for (String line : Files.lines(Paths.get("src/main/resources/locations.txt")).collect(Collectors.toList())) {
                Location location = new Location();
                List<String> list = Arrays.asList(line.split(","));
                location.setType(list.get(0));
                location.setName(list.get(1));
                location.setxCoordinate(Integer.valueOf(list.get(2)));
                location.setyCoordinate(Integer.valueOf(list.get(3)));
                locations.add(location);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return locations;
    }

    public void addNewLocation(Location location) {
        locations.add(location);
        String loc = "";
        for (Location item : locations) {
            loc += item.getType() + "," + item.getName() + "," + item.getxCoordinate() + "," + item.getyCoordinate() + "\n";
        }
        try {
            Files.write(Paths.get("src/main/resources/locations.txt"), loc.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Location> getLocations() {
        return locations;
    }
}
