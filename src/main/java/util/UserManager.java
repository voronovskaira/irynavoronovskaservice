package util;

import model.Location;
import model.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UserManager {
    private List<User> users;

    public UserManager() {
        this.users = readUsers();
    }

    public List<User> readUsers() {
        users = new ArrayList<>();
        try {
            for (String line : Files.lines(Paths.get("src/main/resources/friends.txt")).collect(Collectors.toList())) {
                Location location = new Location();
                List<String> list = Arrays.asList(line.split(","));
                location.setType(list.get(1));
                location.setName(list.get(2));
                location.setxCoordinate(Integer.valueOf(list.get(3)));
                location.setyCoordinate(Integer.valueOf(list.get(4)));
                User user = new User(list.get(0), location);
                users.add(user);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return users;
    }

    public void changeUserLocation(User user) {
        users = users.stream()
                .peek(x -> {
                    if (x.getName().equals(user.getName()))
                        x.setLocation(user.getLocation());
                }).collect(Collectors.toList());
        String usersStr = "";
        for (User item : users) {
            usersStr += item.getName() + "," + item.getLocation().getType() + "," + item.getLocation().getName() + ","
                    + item.getLocation().getxCoordinate() + "," + item.getLocation().getyCoordinate() + "\n";
        }
        try {
            Files.write(Paths.get("src/main/resources/friends.txt"), usersStr.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<User> getUsers() {
        return users;
    }
}
